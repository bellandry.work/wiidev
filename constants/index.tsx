import {
  BatteryCharging,
  BotMessageSquare,
  Cloud,
  Database,
  Fingerprint,
  Globe,
  GlobeLock,
  Layout,
  PlugZap,
  ServerCog,
  ShieldHalf,
  Smartphone,
} from "lucide-react";

import user1 from "../public/assets/profile-pictures/user1.jpg";
import user2 from "../public/assets/profile-pictures/user2.jpg";
import user3 from "../public/assets/profile-pictures/user3.jpg";
// import user4 from "../assets/profile-pictures/user4.jpg";
// import user5 from "../assets/profile-pictures/user5.jpg";
// import user6 from "../assets/profile-pictures/user6.jpg";

export const navItems = [
  { label: "Accueil", link: "/#hero" },
  { label: "Services", link: "/#services" },
  { label: "A propos", link: "/#about" },
  { label: "Contact", link: "/#contact" },
  { label: "Blog", link: "/blog" },
];

export const slugs = [
  "typescript",
  "javascript",
  "dart",
  "react",
  "flutter",
  "android",
  "html5",
  "css3",
  "nodedotjs",
  "express",
  "nextdotjs",
  "prisma",
  "amazonaws",
  "postgresql",
  "firebase",
  "nginx",
  "vercel",
  "testinglibrary",
  "jest",
  "cypress",
  "docker",
  "git",
  "jira",
  "github",
  "gitlab",
  "visualstudiocode",
  "androidstudio",
  "sonarqube",
  "figma",
];

export const testimonials = [
  {
    user: "Anaelle Belinga",
    company: "Stellar Solutions",
    image: user1,
    text: "Je suis extrêmement satisfait des services fournis. L'équipe a été réactive, professionnelle et a livré des résultats au-delà de mes attentes.",
  },
  {
    user: "Brice Semevo",
    company: "Blue Horizon Technologies",
    image: user2,
    text: "Je ne pourrais pas être plus heureux du résultat de notre projet. La créativité et les compétences en résolution de problèmes de l'équipe ont été essentielles pour donner vie à notre vision.",
  },
  {
    user: "David Johnson",
    company: "Quantum Innovations",
    image: user3,
    text: "Travailler avec Wiidev a été un plaisir. Leur souci du détail et leur engagement envers l'excellence sont louables. Je les recommanderais vivement à quiconque recherche un service de première qualité",
  },
  // {
  //   user: "Ronee Brown",
  //   company: "Fusion Dynamics",
  //   image: user4,
  //   text: "Working with the team at XYZ Company was a game-changer for our project. Their attention to detail and innovative solutions helped us achieve our goals faster than we thought possible. We are grateful for their expertise and professionalism!",
  // },
  // {
  //   user: "Michael Wilson",
  //   company: "Visionary Creations",
  //   image: user5,
  //   text: "I am amazed by the level of professionalism and dedication shown by the team. They were able to exceed our expectations and deliver outstanding results.",
  // },
  // {
  //   user: "Emily Davis",
  //   company: "Synergy Systems",
  //   image: user6,
  //   text: "The team went above and beyond to ensure our project was a success. Their expertise and dedication are unmatched. I look forward to working with them again in the future.",
  // },
];

export const features = [
  {
    icon: <BotMessageSquare />,
    text: "Drag-and-Drop Interface",
    description:
      "Easily design and arrange your VR environments with a user-friendly drag-and-drop interface.",
  },
  {
    icon: <Fingerprint />,
    text: "Multi-Platform Compatibility",
    description:
      "Build VR applications that run seamlessly across multiple platforms, including mobile, desktop, and VR headsets.",
  },
  {
    icon: <ShieldHalf />,
    text: "Built-in Templates",
    description:
      "Jumpstart your VR projects with a variety of built-in templates for different types of applications and environments.",
  },
  {
    icon: <BatteryCharging />,
    text: "Real-Time Preview",
    description:
      "Preview your VR application in real-time as you make changes, allowing for quick iterations and adjustments.",
  },
  {
    icon: <PlugZap />,
    text: "Collaboration Tools",
    description:
      "Work together with your team in real-time on VR projects, enabling seamless collaboration and idea sharing.",
  },
  {
    icon: <GlobeLock />,
    text: "Analytics Dashboard",
    description:
      "Gain valuable insights into user interactions and behavior within your VR applications with an integrated analytics dashboard.",
  },
];

export const services = [
  {
    title: "Data Science",
    description:
      "Transformez vos données en insights exploitables pour des décisions stratégiques grâce à nos solutions de data science.",
    icon: <Database />,
  },
  {
    title: "Cloud Migration",
    description:
      "Modernisez votre infrastructure en migrant vers le cloud pour une meilleure scalabilité, sécurité et performance.",
    icon: <Cloud />,
  },
  {
    title: "DevOps",
    description:
      "Optimisez vos processus de développement et déploiement avec nos pratiques DevOps pour une livraison rapide et fiable.",
    icon: <ServerCog />,
  },
  {
    title: "Développement Web",
    description:
      "Créez des sites web performants, sécurisés et adaptés à vos besoins pour une présence en ligne optimale.",
    icon: <Globe />,
  },
  {
    title: "Développement Mobile",
    description:
      "Développez des applications mobiles intuitives et performantes pour iOS et Android, offrant une expérience utilisateur exceptionnelle.",
    icon: <Smartphone />,
  },
  {
    title: "UI/UX Design",
    description:
      "Améliorez l'expérience utilisateur avec des interfaces intuitives et attractives, alignées sur votre identité de marque.",
    icon: <Layout />,
  },
];

export const stats = [
  {
    id: "stats-2",
    title: "Années d'expertise",
    value: 5,
  },
  {
    id: "stats-3",
    title: "Projets réalisés",
    value: 30,
  },
  {
    id: "stats-1",
    title: "Clients satisfaits",
    value: 10,
  },
];

export const checklistItems = [
  {
    title: "Code merge made easy",
    description:
      "Track the performance of your VR apps and gain insights into user behavior.",
  },
  {
    title: "Review code without worry",
    description:
      "Track the performance of your VR apps and gain insights into user behavior.",
  },
  {
    title: "AI Assistance to reduce time",
    description:
      "Track the performance of your VR apps and gain insights into user behavior.",
  },
  {
    title: "Share work in minutes",
    description:
      "Track the performance of your VR apps and gain insights into user behavior.",
  },
];

export const pricingOptions = [
  {
    title: "Free",
    price: "$0",
    features: [
      "Private board sharing",
      "5 Gb Storage",
      "Web Analytics",
      "Private Mode",
    ],
  },
  {
    title: "Pro",
    price: "$10",
    features: [
      "Private board sharing",
      "10 Gb Storage",
      "Web Analytics (Advance)",
      "Private Mode",
    ],
  },
  {
    title: "Enterprise",
    price: "$200",
    features: [
      "Private board sharing",
      "Unlimited Storage",
      "High Performance Network",
      "Private Mode",
    ],
  },
];

export const resourcesLinks = [
  { href: "#", text: "Getting Started" },
  { href: "#", text: "Documentation" },
  { href: "#", text: "Tutorials" },
  { href: "#", text: "API Reference" },
  { href: "#", text: "Community Forums" },
];

export const platformLinks = [
  { href: "#", text: "Features" },
  { href: "#", text: "Supported Devices" },
  { href: "#", text: "System Requirements" },
  { href: "#", text: "Downloads" },
  { href: "#", text: "Release Notes" },
];

export const communityLinks = [
  { href: "#", text: "Events" },
  { href: "#", text: "Meetups" },
  { href: "#", text: "Conferences" },
  { href: "#", text: "Hackathons" },
  { href: "#", text: "Jobs" },
];
