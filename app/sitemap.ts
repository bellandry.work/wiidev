import { client } from "@/sanity/lib/client";
import { Post } from "common-types";
import { MetadataRoute } from "next";
import { groq } from "next-sanity";

export default async function sitemap(): Promise<MetadataRoute.Sitemap> {
  const baseUrl = "https://wiidev.fr";

  const query = groq`
    *[_type == 'post'] {
      ...,
      author->,
      categories[]->
    } | order(_createdAt desc)
  `;
  const posts: Post[] = await client.fetch(query);

  const postEntries: MetadataRoute.Sitemap = posts.map((post) => ({
    url: `${baseUrl}/blog/article/${post.slug.current}`,
    lastModified: new Date(post._createdAt),
    changeFrequency: "monthly",
    priority: 0.8,
  }));

  return [
    {
      url: `${baseUrl}`,
      lastModified: new Date(),
      changeFrequency: "monthly",
      priority: 0.8,
    },
    {
      url: `${baseUrl}/blog`,
      lastModified: new Date(),
      changeFrequency: "weekly",
      priority: 0.5,
    },
    ...postEntries,
  ];
}
