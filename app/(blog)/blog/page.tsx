import BlogPosts from "@/components/blog/blog-posts";
import { NewsLetterCta } from "@/components/news-letter-cta";
import { client } from "@/sanity/lib/client";
import { groq } from "next-sanity";

const getPosts = async () => {
  const query = groq`
    *[_type == 'post'] {
      ...,
      author->,
      categories[]->
    } | order(_createdAt desc)
  `;
  const datas = await client.fetch(query);
  return datas;
};

// - ISR
// revalidate this page every 60 seconds
export const revalidate = 60;

const Blog = async () => {
  const posts = await getPosts();
  return (
    <div className="px-4 sm:px-4 md:container">
      <BlogPosts posts={posts} />
      <section className="mb-24">
        <NewsLetterCta />
      </section>
    </div>
  );
};

export default Blog;
