import urlFor from "@/lib/urlFor";
import { client } from "@/sanity/lib/client";
import { PortableText } from "@portabletext/react";
import { groq } from "next-sanity";
import Image from "next/image";

import BlogPosts from "@/components/blog/blog-posts";
import { RichTextComponent } from "@/components/blog/rich-text-component";
import { NewsLetterCta } from "@/components/news-letter-cta";
import { NotFound } from "@/components/not-found";
import type { Post } from "common-types";
import { Metadata } from "next";
import { cache } from "react";

type Props = {
  params: {
    slug: string;
  };
};

// - ISR
// revalidate this page every 60 seconds
export const revalidate = 60;

export async function generateStaticParams() {
  const query = groq`
    *[_type == 'post'] {
      slug
    }
  `;
  const slugs: Post[] = await client.fetch(query);
  const slugRoutes = slugs.map((slug) => slug.slug.current);

  return slugRoutes.map((slug) => ({
    slug,
  }));
}

const getPost = cache(async (slug: string) => {
  const query = groq`
    *[_type == 'post' && slug.current == $slug][0] {
      ...,
      author->,
      categories[]->,
    }
  `;

  const post: Post = await client.fetch(query, { slug });
  return post;
});

const getPosts = async (slug: string) => {
  const query = groq`
    *[_type == 'post' && slug.current != $slug] {
      ...,
      author->,
      categories[]->
    } | order(_createdAt desc)
  `;
  const datas = await client.fetch(query, { slug });
  return datas;
};

export async function generateMetadata({
  params: { slug },
}: Props): Promise<Metadata> {
  const post = await getPost(slug);
  if (!post) {
    return {
      title: "404 Not found",
      description: "Cet article n'existe pas ou a été supprimé",
    };
  }

  return {
    title: post?.title,
    description: post?.description,
    keywords: [
      ...post.categories.map((category) => category.title),
      "wiidev",
      "blog",
      "agence",
      "devops",
    ],
    openGraph: {
      images: [
        {
          url: urlFor(post.mainImage).url(),
          width: 1200,
          height: 630,
        },
      ],
    },
  };
}

async function PostPage({ params: { slug } }: Props) {
  const post = await getPost(slug);
  if (!post) {
    return <NotFound />;
  }
  const posts = await getPosts(slug);

  return (
    post && (
      <>
        <article className="px-6 md:container">
          <section className="space-y-2 border border-emphasize">
            <div className="min-h-56 relative flex flex-col justify-between md:flex-row">
              <div className="absolute top-0 h-full w-full p-10 opacity-20 blur-sm">
                {post && post.mainImage && (
                  <Image
                    className="mx-auto object-cover object-center"
                    src={urlFor(post.mainImage).url()}
                    alt={post.author.name}
                    fill
                  />
                )}
              </div>

              <section className="w-full bg-emphasize p-5">
                <div className="flex flex-col justify-between gap-y-5 md:flex-row">
                  <div>
                    <h1 className="text-4xl font-extrabold mb-2 md:mb-4">
                      {post?.title}
                    </h1>
                    <p>
                      Publié le{" "}
                      {new Date(post._createdAt).toLocaleDateString("fr-FR", {
                        day: "numeric",
                        month: "long",
                        year: "numeric",
                      })}
                    </p>
                  </div>

                  <div className="flex items-center space-x-2 opacity-100">
                    {post.author.image && (
                      <Image
                        className="h-10 w-10 rounded-full"
                        src={urlFor(post.author.image).url()}
                        alt={post.author.name}
                        width={40}
                        height={40}
                      />
                    )}
                    <div className="w-64">
                      <h3 className="text-lg font-bold">
                        <span className="mr-1 font-normal">Par</span>{" "}
                        {post.author.name}
                      </h3>
                      <div className="text-md text-muted-foreground">
                        <PortableText
                          value={post.author?.bio}
                          components={RichTextComponent}
                        />
                      </div>
                    </div>
                  </div>
                </div>

                <div>
                  <h2 className="pt-10 italic">{post.description}</h2>
                  <div className="mt-auto flex items-center justify-end space-x-2">
                    {post.categories.map((category) => (
                      <p
                        key={category._id}
                        className="mt-4 rounded-full bg-gray-800 px-3 py-1 text-sm font-semibold text-white"
                      >
                        {category.title}
                      </p>
                    ))}
                  </div>
                </div>
              </section>
            </div>
          </section>
          <div className="max-w-[1200px] mx-auto mt-4 md:mt-8">
            <PortableText value={post.body} components={RichTextComponent} />
          </div>
        </article>
        <section className="mx-6 md:container my-14">
          <NewsLetterCta />
        </section>
        <section className="px-6 md:container">
          <h2 className="text-3xl font-bold">Autres articles</h2>
          <BlogPosts posts={posts} check={true} />
        </section>
      </>
    )
  );
}
export default PostPage;
