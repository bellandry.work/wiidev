import { ThemeProvider } from "@/components/theme-provider";
import { cn } from "@/lib/utils";
import type { Metadata } from "next";
import { Inter as FontSans } from "next/font/google";
import "../globals.css";

const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
});
const dev = process.env.NODE_ENV === "development";

export const metadata: Metadata = {
  metadataBase: new URL(
    dev ? "https://7n6pa3k36tsetgzwmt5hfk56ku.srv.us/" : "https://wiidev.fr"
  ),
  title: {
    template: "%s | WiiDev Innovons ensemble",
    default: "WiiDev - Innovons Ensemble, Construisons le Futur Digital",
  },
  description:
    "WiiDev, transformons vos rêves en réalités. Spécialisés en développement web et mobile, data science, cloud migration, DevOps et UI/UX design, Wiidev est votre partenaire pour des solutions digitales innovantes.",
  keywords: [
    "wiidev",
    "data science",
    "devops",
    "ui/ux design",
    "cloud",
    "cloud-migration",
    "react development",
    "web development",
    "portfolio",
    "web design",
    "front-end",
    "back-end",
    "mobile development",
    "software engeneering",
    "Next.js Development",
    "Laravel app",
  ],
  twitter: {
    card: "summary_large_image",
    creator: "laclass.dev",
  },
  category: "technology",
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="fr">
      <body
        className={cn(
          "min-h-screen bg-background font-sans antialiased",
          fontSans.variable
        )}
      >
        <ThemeProvider attribute="class" defaultTheme="system" enableSystem>
          {children}
        </ThemeProvider>
      </body>
    </html>
  );
}
