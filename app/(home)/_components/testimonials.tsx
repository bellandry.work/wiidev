import { testimonials } from "@/constants";
import Image from "next/image";

const Testimonials = () => {
  return (
    <section id="testimonials" className="px-4 md:container">
      <div className="relative mt-14 md:mt-16">
        <div className="text-center">
          <span className="bg-gray-700 dark:bg-neutral-700 text-orange-400 dark:text-orange-500 border-neutral-400 dark:border-neutral-800 rounded-full h-6 text-sm font-medium px-4 py-1 uppercase">
            Témoignages
          </span>
          <h2 className="text-3xl sm:text-5xl mt-10 tracking-wide font-semibold">
            Ils nous ont fait
            <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text">
              {" "}
              confiance
            </span>
          </h2>
          <p className="my-6 text-center text-md md:text-lg text-neutral-800 dark:text-neutral-300">
            Découvrez les avis de clients nous ayant confiés leurs projets
          </p>
        </div>
        <div className="flex lg:gap-2 lg:flex-nowrap flex-wrap mt-12 md:mt-14 justify-center">
          {testimonials.map((testimonial, index) => (
            <div
              key={index}
              className="w-full items-center flex sm:w-1/2 lg:w-1/3 my-3"
            >
              <div className="flex h-full group bg-gradient-to-tr from-slate-300/20 to-slate-300/80 backdrop-blur-lg shadow-sm dark:bg-gradient-to-br dark:from-slate-600/60 dark:to-slate-600/10 transition-all cursor-pointer rounded-lg m-1 py-6 px-4">
                <div className="mr-3">
                  <div className="relative h-14 w-14 rounded-full">
                    <Image
                      src={testimonial.image}
                      alt={testimonial.user}
                      fill
                      style={{ objectFit: "cover" }}
                      className="rounded-full"
                    />
                  </div>
                </div>
                <div className="w-4/5">
                  <h5 className="mt-1/2 mb-1 font-semibold text-xl dark:text-neutral-100">
                    {testimonial.user}
                  </h5>
                  <p className="mb-4 text-gray-700 dark:text-neutral-400">
                    {testimonial.company}
                  </p>
                  <p className="text-muted-foreground">{testimonial.text}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Testimonials;
