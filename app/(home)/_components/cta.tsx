import { Button } from '@/components/ui/button'
import Link from 'next/link'

const CTA = () => {
  return (
    <section id="cta" className="px-4 md:container">
      <div className="my-12 w-full p-8 md:p-14 flex flex-col md:flex-row justify-between md:items-center gap-4 rounded-3xl bg-gradient-to-r from-slate-700 to-slate-700/80 text-white dark:from-slate-800 dark:to-slate-500">
        <div className='4/6'>
          <h2 className="text-4xl">Démarrer maintenant</h2>
          <p className="text-md text-slate-200">
            Entrez en contact avec nous et demandez un devi, c&pas;est gratuit !
          </p>
        </div>
        <div className='w-2/6 md:text-right'>
          <Link href="/contact">
            <Button size={"lg"} className='bg-gradient-to-r from-orange-400 to-orange-600 hover:from-slate-300 dark:hover:from-slate-600 rounded-xl'>Demander un devi</Button>
          </Link>
        </div>
      </div>
    </section>
  )
}

export default CTA