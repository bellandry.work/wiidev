"use client";

import { LogoMobile } from "@/components/logo";
import { navItems } from "@/constants";
import Link from "next/link";

const Footer = () => {
  return (
    <>
      <section id="contact">
        <div className="px-4 md:container mb-10 mt-10">
          <div className="flex flex-col sm:flex-row gap-4">
            <div className="w-full sm:w-1/2 md:w-1/3 space-y-6 border-b sm:border-b-0 border-orange-500">
              <LogoMobile />
              <p className="text-muted-foreground pb-6">
                WiiDev transforme vos idées en solutions digitales innovantes et
                sur mesure. Spécialisés en développement web, mobile et data
                science, nous propulsons votre entreprise vers un succès durable
                et compétitif.
              </p>
            </div>
            <div className="flex flex-row gap-4 w-full sm:w-1/2 md:w-2/3 justify-evenly">
              <div className="w-1/2 md:w-2/6 flex flex-col">
                {navItems.map((item, index) => (
                  <Link
                    href={item.link}
                    key={index}
                    className="hover:font-semibold justify-start text-md text-neutral-700 dark:text-gray-400 md:hover:text-foreground py-2 hover:border-b-orange-500 hover:border-b-2 w-fit"
                  >
                    {item.label}
                  </Link>
                ))}
              </div>
              <div className="w-1/2 md:w-3/6 flex flex-col">
                <Link
                  href="/privacy-policy"
                  className="md:font-semibold w-fit justify-start text-md text-neutral-700 dark:text-gray-400 md:hover:text-foreground py-2 hover:border-b-orange-500 hover:border-b-2 transition-all"
                >
                  Politique de confidentialité
                </Link>
              </div>
            </div>
          </div>
        </div>
        <div className="text-center py-6 text-white bg-slate-950 border-t dark:border-t-orange-500">
          <p>©2024 WiiDev - Tous droits réservés</p>
        </div>
      </section>
    </>
  );
};

export default Footer;
