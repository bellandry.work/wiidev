import IconCloud from "@/components/magicui/icon-cloud"
import { services, slugs } from "@/constants"

const ServiceSection = () => {
  return (
    <section id="services" className="px-4 md:container">
      <div className="relative mt-14 md:mt-16">
        <div className="text-center">
          <span className="bg-gray-700 dark:bg-neutral-700 text-orange-400 dark:text-orange-500 border-neutral-400 dark:border-neutral-800 rounded-full h-6 text-sm font-medium px-4 py-1 uppercase">
            Services
          </span>
          <h2 className="text-3xl sm:text-5xl mt-10 tracking-wide font-semibold">
            Découvrez nos
            <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text">{" "}services</span>
          </h2>
        </div>
        <div className="flex flex-wrap mt-12 md:mt-14">
          {services.map((service, index) => (
            <div key={index} className="w-full sm:w-1/2 lg:w-1/3">
              <div className="flex group bg-neutral-600/10 md:bg-inherit hover:md:bg-neutral-600/10 dark:bg-slate-800/30 dark:md:bg-inherit md:hover:dark:bg-slate-800/30 transition-all cursor-pointer rounded-lg m-1 py-6 px-4">
                <div className="flex mr-2 md:mx-4 h-10 w-10 p-2 items-center justify-center rounded-full bg-neutral-300 dark:bg-neutral-700 dark:text-orange-500 group-hover:bg-neutral-700 group-hover:text-orange-500 dark:group-hover:bg-orange-500 dark:group-hover:text-slate-300 transition-all">
                  {service.icon}
                </div>
                <div>
                  <h5 className="mt-1 mb-6 text-xl dark:text-neutral-100">{service.title}</h5>
                  <p className="text-muted-foreground">{service.description}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
        <IconCloud iconSlugs={slugs} />
      </div>
    </section>
  )
}

export default ServiceSection