import { Tabs, TabsContent, TabsList, TabsTrigger } from "@/components/ui/tabs";
import Image from "next/image";

const AboutUs = async () => {
  return (
    <section id="about" className="px-4 md:container pt-14">
      <div className="text-center w-full">
        <span className="bg-gray-700 dark:bg-neutral-700 text-orange-400 dark:text-orange-500 border-neutral-400 dark:border-neutral-800 rounded-full h-6 text-sm font-medium px-4 py-1 uppercase">
          A propos
        </span>
        <h2 className="text-3xl sm:text-5xl mt-10 tracking-wide font-semibold">
          Qui sommes
          <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text">
            {" "}
            nous ?
          </span>
        </h2>
      </div>
      <div className="flex flex-col-reverse md:flex-row gap-4 relative mt-8 md:mt-12">
        <div className="w-full relative md:w-1/2 aspect-video">
          <Image
            src={"/assets/about-pic.jpg"}
            alt="equipe développeurs"
            fill
            style={{ objectFit: "cover" }}
            className="rounded-xl"
          />
        </div>
        <div className="w-full md:w-1/2">
          <Tabs defaultValue="about">
            <TabsList className="flex w-fit gap-2 md:gap-4 bg-transparent">
              <TabsTrigger
                className="data-[state=active]:bg-gradient-to-r data-[state=active]:from-orange-400 data-[state=active]:to-orange-600 data-[state=active]:text-slate-50 text-md dark:bg-slate-900 bg-slate-200/80"
                value="about"
              >
                A propos
              </TabsTrigger>
              <TabsTrigger
                className="data-[state=active]:bg-gradient-to-r data-[state=active]:from-orange-400 data-[state=active]:to-orange-600 data-[state=active]:text-slate-50 text-md dark:bg-slate-900 bg-slate-200/80"
                value="vision"
              >
                Notre vision
              </TabsTrigger>
              <TabsTrigger
                className="data-[state=active]:bg-gradient-to-r data-[state=active]:from-orange-400 data-[state=active]:to-orange-600 data-[state=active]:text-slate-50 text-md dark:bg-slate-900 bg-slate-200/80"
                value="engagement"
              >
                Engagement
              </TabsTrigger>
            </TabsList>
            <TabsContent value="about">
              <p className="text-md text-muted-foreground">
                <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text font-semibold">
                  WiiDev
                </span>{" "}
                est une équipe passionnée de professionnels spécialisés en
                développement web, mobile et data science. Notre mission est de
                transformer vos idées en solutions digitales innovantes et
                performantes. Grâce à notre expertise et notre approche sur
                mesure, nous propulsons votre entreprise vers de nouveaux
                sommets tout en assurant une qualité irréprochable et une
                performance optimale.
              </p>
            </TabsContent>
            <TabsContent value="vision">
              <p className="text-md text-muted-foreground">
                Nous croyons en un futur digital où chaque entreprise, quelle
                que soit sa taille, peut atteindre son plein potentiel. Grâce à
                des solutions technologiques de pointe, nous visons à créer des
                opportunités et à innover constamment pour vous offrir un
                avantage compétitif durable, en vous accompagnant à chaque étape
                de votre transformation digitale.
              </p>
            </TabsContent>
            <TabsContent value="engagement">
              <p className="text-md text-muted-foreground">
                Nous nous engageons à offrir un service personnalisé, en nous
                basant sur une compréhension profonde de vos besoins spécifiques
                et en adaptant nos solutions pour répondre à vos objectifs
                uniques. Chaque projet est une collaboration, et nous mettons
                tout en œuvre pour garantir des résultats exceptionnels et
                durables, assurant ainsi votre satisfaction et votre succès
                continu dans un environnement en constante évolution.
              </p>
            </TabsContent>
          </Tabs>
        </div>
      </div>
    </section>
  );
};

export default AboutUs;
