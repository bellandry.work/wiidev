import { Button } from "@/components/ui/button";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { Textarea } from "@/components/ui/textarea";
import Image from "next/image";

const Contact = () => {
  return (
    <section id="contact" className="px-4 md:container mb-24">
      <div className="relative mt-14 md:mt-16">
        <div className="text-center">
          <span className="bg-gray-700 dark:bg-neutral-700 text-orange-400 dark:text-orange-500 border-neutral-400 dark:border-neutral-800 rounded-full h-6 text-sm font-medium px-4 py-1 uppercase">
            contact
          </span>
          <h2 className="text-3xl sm:text-5xl mt-10 tracking-wide font-semibold">
            Nous
            <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text">
              {" "}
              Contacter
            </span>
          </h2>
          <p className="my-6 text-center text-md md:text-lg text-neutral-800 dark:text-neutral-300">
            Demandez un devi, c'est gratuit !
          </p>
        </div>
        <div className="flex flex-col md:flex-row gap-4 relative mt-8 md:mt-12">
          <div className="w-full relative md:w-1/2 aspect-video">
            <Image
              src={"/assets/contact-pic.jpeg"}
              alt="image contact"
              fill
              style={{ objectFit: "cover" }}
              className="rounded-xl"
            />
          </div>
          <div className="w-full md:w-1/2 mx-auto max-w-[500px] space-y-8">
            <div className="space-y-2 text-center">
              <p className="text-gray-500 dark:text-gray-400 md:text-md">
                Remplissez le formulaire ci-dessous et nous vous répondrons dans
                les plus brefs délais.
              </p>
            </div>
            <div className="space-y-4">
              <div className="grid grid-cols-2 gap-4">
                <div className="space-y-2">
                  <Label htmlFor="first-name" className="md:text-md">
                    Prénom
                  </Label>
                  <Input id="first-name" placeholder="Entrez votre prénom" />
                </div>
                <div className="space-y-2">
                  <Label htmlFor="last-name" className="md:text-md">
                    Nom
                  </Label>
                  <Input id="last-name" placeholder="Entrez votre nom" />
                </div>
              </div>
              <div className="space-y-2">
                <Label htmlFor="email" className="md:text-md">
                  Email
                </Label>
                <Input
                  id="email"
                  type="email"
                  placeholder="Entrez votre email"
                />
              </div>
              <div className="space-y-2">
                <Label htmlFor="message" className="md:text-md">
                  Message
                </Label>
                <Textarea
                  id="message"
                  placeholder="Entrez votre message"
                  className="min-h-[100px]"
                />
              </div>
              <Button className="ring-1 ring-orange-500 bg-orange-500 hover:text-orange-500 hover:bg-inherit text-white w-full">
                Envoyer
              </Button>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
