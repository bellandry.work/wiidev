'use client'

import { stats } from '@/constants'
import CountUp from 'react-countup'
import VisibilitySensor from 'react-visibility-sensor'

const Stats = () => {
  return (
    <section id="stats" className="px-4 md:container">
      <div className="relative mt-14 md:mt-16">
        <div className="text-center">
          <span className="bg-gray-700 text-orange-400 dark:text-orange-500 dark:bg-neutral-700 border-neutral-400 dark:border-neutral-800 rounded-full h-6 text-sm font-medium px-4 py-1 uppercase">
            stats
          </span>
          <h2 className="text-3xl sm:text-5xl mt-10 tracking-wide font-semibold">
            Quelques
            <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text">{" "}chiffres</span>
          </h2>
        </div>
        <div className="flex flex-wrap mb-6 sm:mb-20 mt-8 md:mt-10">
          {stats.map((stat, index) => (
            <div key={index} className="flex flex-1 justify-center items-center flex-row m-3">
              <h4 className="font-poppins font-semibold text-4xl md:text-5xl text-orange-900">
                +
                <CountUp end={stat.value} redraw={true}>
                  {({ countUpRef, start }) => (
                    <VisibilitySensor onChange={start} delayedCall>
                      <span ref={countUpRef} />
                    </VisibilitySensor>
                  )}
                </CountUp>
              </h4>
              <p className="font-poppins font-normal uppercase ml-3 text-xl md:text-2xl dark:bg-gradient-to-r dark:from-orange-200 dark:to-orange-50 dark:bg-clip-text dark:text-transparent">{stat.title}</p>
            </div>
          ))}
        </div>
      </div>
    </section>
  )
}

export default Stats