import { Button } from "@/components/ui/button"
import Image from "next/image"
import Link from "next/link"

const HeroSection = () => {
  return (
    <section id="hero" className="relative w-full md:max-h-[calc(100vh-100px)] min-h-[calc(100vh-150px)] md:min-h-[calc(100vh-300px)]  aspect-square md:aspect-video">
      <Image src={"/assets/team-hero.jpg"} alt="equipe développeurs" fill className="object-cover" />
      <div className="absolute inset-0 flex items-center bg-slate-200 dark:bg-slate-950 bg-opacity-75 dark:bg-opacity-70 md:bg-transparent dark:md:bg-transparent md:bg-gradient-to-r dark:from-slate-950 from-slate-200 px-4">
        <div className="md:container">
          <div className="w-full md:w-7/12 flex flex-col gap-8 text-center dark:text-white md:text-left">
            <h1 className="text-5xl md:text-7xl font-semibold leading-tight">Innovons <span className="bg-gradient-to-r from-amber-500 to-orange-700 bg-clip-text font-bold text-transparent">Ensemble</span></h1>
            <p className="text-md md:text-lg w-5/6 mx-auto md:mx-0">
              Une approche unique, centrée sur vous. Notre promesse ? Des solutions digitales sur mesure qui catapultent votre entreprise vers la réussite.
            </p>
            <div className="flex items-center justify-center md:justify-start gap-2 transition-all">
              <Link href={"#contact"} >
                <Button size={"lg"} className="bg-orange-600 text-white hover:bg-inherit hover:text-orange-600 ring-1 ring-orange-500">
                  Nous Contacter
                </Button>
              </Link>
              <Link href={"#contact"} >
                <Button size={"lg"} variant={"ghost"} className="ring-orange-500 ring-1 text-orange-600 hover:text-white hover:bg-orange-500">
                  Demander un devi
                </Button>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default HeroSection