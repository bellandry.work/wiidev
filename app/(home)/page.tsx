import Navbar from "@/components/navbar/navbar";
import AboutUs from "./_components/about-us";
import Contact from "./_components/contact";
import CTA from "./_components/cta";
import Footer from "./_components/footer";
import HeroSection from "./_components/hero-section";
import ServiceSection from "./_components/services-section";
import Stats from "./_components/stats";
import Testimonials from "./_components/testimonials";

export default function Home() {
  return (
    <main className="relative flex h-screen w-full flex-col" >
      <Navbar />
      <div className="w-full">
        <HeroSection />
        <ServiceSection />
        <AboutUs />
        <Stats />
        <CTA />
        <Testimonials />
        <Contact />
        <Footer />
      </div>
    </main>
  );
}
