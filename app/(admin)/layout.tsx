export const metadata = {
  title: "Wiidev Studio",
  description: "Admin space to manage wiidev blog contents",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
