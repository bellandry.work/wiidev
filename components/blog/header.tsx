"use client";

import Link from "next/link";
import { usePathname } from "next/navigation";
import Logo from "../logo";
import { ThemeSwitcherBtn } from "../theme-switcher-btn";

function Header() {
  const pathName = usePathname();
  const isActive = pathName === "/blog";

  return (
    <header className="flex items-center justify-between space-x-2 px-4 sm:px-6 md:container py-4 font-bold">
      <div className="flex items-center space-x-2">
        <Logo />
      </div>
      <div className="flex gap-4 sm:gap-6 md:gap-8">
        {!isActive && (
          <div className="relative flex items-center group">
            <Link
              href={"/blog"}
              className="font-semibold text-muted-foreground transition-all group-hover:text-slate-800 dark:group-hover:text-white"
            >
              Accueil
            </Link>
          </div>
        )}
        <ThemeSwitcherBtn />
      </div>
    </header>
  );
}
export default Header;
