import urlFor from "@/lib/urlFor";
import { Post } from "common-types";
import { ArrowUpRightIcon } from "lucide-react";
import Image from "next/image";

interface PostItemProps {
  post: Post;
}

const PostItem = ({ post }: PostItemProps) => {
  return (
    <div>
      <div className="group flex cursor-pointer flex-col">
        <div className="relative aspect-video h-64 sm:h-80 w-full transform-gpu drop-shadow-xl transition-transform duration-200 ease-out group-hover:scale-105">
          <Image
            className="object-cover object-left lg:object-center rounded-sm"
            src={urlFor(post.mainImage).url()}
            alt={post.author.name}
            fill
          />
          <div className="absolute bottom-0 flex w-full justify-between rounded bg-neutral-900 bg-opacity-20 p-5 text-white drop-shadow-lg backdrop-blur-lg">
            <div>
              <p className="font-bold">{post?.title}</p>
              <p className="transfor m-gpu subpixel-antialiased">
                {new Date(post._createdAt).toLocaleDateString("fr-FR", {
                  day: "numeric",
                  month: "long",
                  year: "numeric",
                })}
              </p>
            </div>

            <div className="flex flex-col gap-y-2 md:flex-row md:gap-x-2">
              {post.categories.map((category) => (
                <div
                  key={category._id}
                  className="h-fit rounded-full bg-orange-400 px-3 py-1 text-center text-sm font-semibold text-black"
                >
                  <p>{category.title}</p>
                </div>
              ))}
            </div>
          </div>
        </div>

        <div className="mt-3 md:mt-5 flex-1">
          <p className="text-lg font-bold underline">{post.title}</p>
          <p className="text-gray-500 line-clamp-2">{post.description}</p>
        </div>

        <p className="mt-5 flex items-center font-bold group-hover:underline">
          Lire l&apos;article
          <ArrowUpRightIcon className="ml-2 h-4 w-4" />
        </p>
      </div>
    </div>
  );
};

export default PostItem;
