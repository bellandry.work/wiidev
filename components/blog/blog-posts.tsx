import { cn } from "@/lib/utils";
import { Post } from "common-types";
import ClientSideRoute from "./client-side-route";
import PostItem from "./post-item";

const BlogPosts = ({ posts, check }: { posts: Post[]; check?: boolean }) => {
  return (
    <div>
      <div>
        <hr className="mb-10" />

        <div
          className={cn(
            "grid grid-cols-1 gap-x-10 gap-y-16 px-4 pb-24 md:grid-cols-2",
            check && "md:grid-cols-3 sm:grid-cols-2"
          )}
        >
          {/* Posts */}
          {posts.map((post) => (
            <ClientSideRoute
              key={post._id}
              route={`/blog/article/${post.slug.current}`}
            >
              <PostItem post={post} />
            </ClientSideRoute>
          ))}
        </div>
      </div>
    </div>
  );
};

export default BlogPosts;
