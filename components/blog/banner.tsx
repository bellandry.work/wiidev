const Banner = () => {
  return (
    <div className="mb-10 flex flex-col justify-between px-4 sm:px-6 md:container py-5 lg:flex-row lg:space-x-5">
      <div>
        <h1 className="text-5xl md:text-6xl font-bold">Blog Hebdo <span className="bg-gradient-to-r from-orange-400 to-orange-600 text-transparent bg-clip-text">WiiDev</span></h1>
        <h2 className="mt-6 md:mt-8 font-semibold">
          Bienvenu à{' '}
          <span className="border-b-2 border-b-orange-400">
            vous !
          </span>
        </h2>
      </div>

      <p className="mt-5 max-w-sm text-gray-400 md:mt-2">
        Actus tech | Astuces | Vulgarisation Tech
        Conseils et plus
      </p>
    </div>
  )
}
export default Banner
