import { ArrowLeftCircleIcon } from 'lucide-react';
import Link from 'next/link';

function StudioNavbar(props: any) {
  return (
    <div className='bg-gray-950/95'>
      <div >
        <Link
          href="/blog"
          style={{ alignItems: 'center', display: 'flex', gap: '5px', color: 'orange', textDecoration: 'none', padding: '1.2rem 0.8rem' }}
        >
          <ArrowLeftCircleIcon className="mr-2 h-6 w-6" />
          Aller au site
        </Link>
        <div></div>
      </div>
      <>{props.renderDefault(props)}</>
    </div>
  )
}
export default StudioNavbar
