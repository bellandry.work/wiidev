"use client";

import { testimonials } from "@/constants";
import { cn } from "@/lib/utils";
import Image from "next/image";

interface AvatarCirclesProps {
  className?: string;
  numPeople?: number;
}

export default function AvatarCircles({
  numPeople,
  className,
}: AvatarCirclesProps) {
  return (
    <div className={cn("z-10 flex -space-x-4 rtl:space-x-reverse", className)}>
      {testimonials.map((item) => (
        <div className="relative h-14 w-14 rounded-full border-2 border-white dark:border-gray-800">
          <Image
            className="rounded-full object-cover"
            src={item.image}
            alt={item.company}
          />
        </div>
      ))}
      {/* <a className="relative flex h-14 w-14 items-center justify-center rounded-full border-2 border-white bg-black text-center text-xs font-medium text-white hover:bg-gray-600 dark:border-gray-800 dark:bg-white dark:text-black">
        +{numPeople}
      </a> */}
    </div>
  );
}
