import Image from "next/image";
import Link from "next/link";
import logoMobile from "../public/assets/logo-mobile.png";
import logo from "../public/assets/logo.svg";

const Logo = () => {
  return (
    <Link href="/" className="flex items-center gap-2 z-10">
      <div className="relative w-6 h-6">
        <Image src={logo} alt="logo wiidev" fill />
      </div>
      <p className="bg-gradient-to-r from-amber-400 to-orange-600 bg-clip-text text-2xl font-bold leading-light tracking-tighter text-transparent">
        WiiDev
      </p>
    </Link>
  );
};

export const LogoMobile = () => {
  return (
    <Link href="/" className="flex items-center gap-2 z-10">
      <div className="relative w-20 h-6">
        <Image src={logoMobile} alt="logo wiidev" fill />
      </div>
    </Link>
  );
};

export default Logo;
