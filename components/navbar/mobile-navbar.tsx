import { Menu } from "lucide-react"
import Link from "next/link"
import { useState } from "react"
import Logo, { LogoMobile } from "../logo"
import { ThemeSwitcherBtn } from "../theme-switcher-btn"
import { Button } from "../ui/button"
import { Sheet, SheetContent, SheetTrigger } from "../ui/sheet"
import NavbarItem from "./navbar-item"

const MobileNavbar = ({ items }: { items: { link: string, label: string }[] }) => {
  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className="block border-separate bg-backgound/30 border-b md:hidden fixed left-0 right-0 top-0 backdrop-blur-md z-50">
      <nav className="container flex items-center justify-between px-4">
        <Sheet open={isOpen} onOpenChange={setIsOpen}>
          <SheetTrigger asChild>
            <Button variant={"ghost"} size={"icon"}>
              <Menu />
            </Button>
          </SheetTrigger>
          <SheetContent className="w-[300px]" side="left">
            <Logo />
            <div className='flex flex-col gap-2 pt-4'>
              {items.map((item) => (
                <NavbarItem
                  key={item.label}
                  label={item.label}
                  link={item.link}
                  clickCallback={() => setIsOpen((open) => !open)}
                />
              ))}
              <Link href={"#contact"}>
                <Button className='ring-1 mt-4 w-full rounded-lg ring-orange-500 text-orange-500 transition-all bg-transparent hover:bg-orange-500 hover:text-white'>Demander un devi</Button>
              </Link>
            </div>
          </SheetContent>
        </Sheet>
        <div className="flex h-[70px] min-h-[60px] items-center gap-x-4">
          <LogoMobile />
        </div>
        <div className="flex items-center gap-2">
          <ThemeSwitcherBtn />
        </div>
      </nav>
    </div>
  )
}

export default MobileNavbar