import { cn } from "@/lib/utils";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { buttonVariants } from "../ui/button";

const NavbarItem = ({ link, label, clickCallback }: { link: string, label: string, clickCallback?: () => void }) => {
  const pathName = usePathname()
  const isActive = pathName === link

  return (
    <div className="relative flex items-center">
      <Link
        href={link}
        className={cn(
          buttonVariants({
            variant: 'ghost'
          }),
          "w-full justify-start text-md text-neutral-700 dark:text-gray-400 md:hover:text-foreground md:hover:bg-inherit py-6 md:py-8 hover:bg-slate-300/30 transition-all",
          isActive && "text-black dark:text-white bg-slate-300/50 md:bg-inherit")}
        onClick={() => {
          if (clickCallback) clickCallback();
        }}
      >
        {label}
      </Link>
      {
        isActive && (
          <div className="absolute -bottom-[4px] left-1/2 h-[2px] w-[80%] -translate-x-1/2 rounded-xl bg-orange-600 hidden md:block" />
        )
      }
    </div>
  )
}

export default NavbarItem