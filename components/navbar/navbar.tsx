"use client";

import { navItems as items } from "@/constants";
import Link from "next/link";
import Logo from "../logo";
import { ThemeSwitcherBtn } from "../theme-switcher-btn";
import { Button } from "../ui/button";
import MobileNavbar from "./mobile-navbar";
import NavbarItem from "./navbar-item";

const Navbar = () => {
  return (
    <>
      <div className="hidden md:block border-separate border-b bg-background/30 fixed top-0 right-0 left-0 backdrop-blur-md z-50">
        <nav className="container relative flex items-center justify-between px-8">
          <div className="flex h-[70px] min-h-[60px] items-center gap-x-4">
            <Logo />
          </div>
          <div>
            <div className="flex h-full">
              {items.map((item) => (
                <NavbarItem
                  key={item.label}
                  link={item.link}
                  label={item.label}
                />
              ))}
            </div>
          </div>
          <div className="flex items-center gap-4">
            <Link href="#contact">
              <Button className="ring-1 rounded-lg ring-orange-500 text-orange-500 transition-all bg-transparent hover:bg-orange-500 hover:text-white">
                Demander un devi
              </Button>
            </Link>
            <ThemeSwitcherBtn />
          </div>
        </nav>
      </div>
      <MobileNavbar items={items} />
    </>
  );
};

export default Navbar;
